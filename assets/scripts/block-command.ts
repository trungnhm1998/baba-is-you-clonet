import GameBlock from "./game-block";
import MapSystem from "./map-system";
import { CONSTANTS } from "./constants";

export abstract class BlockCommand {
  protected xOffset = 0;
  protected yOffset = 0;
  protected mapSystem: MapSystem = null;
  protected currentColIndex = -1;
  protected currentRowIndex = -1;
  constructor(protected gameBlock: GameBlock) {
    this.xOffset = this.yOffset = this.gameBlock.getMoveDistance();
    this.mapSystem = this.gameBlock.mapSystem;
    this.currentRowIndex = this.gameBlock.rowIndex;
    this.currentColIndex = this.gameBlock.columnIndex;
  }

  exec(): void {
    this.resetBlockActionAndPostition();
    // check left right top bottom moveable block and move it and the every moveable block next to it
    this._exec();
    this.updateGameBlockPosition();
  }

  undo(): void {
    this.resetBlockActionAndPostition();
    this._undo();
    this.updateGameBlockPosition();
  }

  protected resetBlockActionAndPostition() {
    const { node } = this.gameBlock;
    cc.tween(node).stop();
    node.stopAllActions();
    let position = this.mapSystem.calculatePositionBasedOnRowAndColumn(this.currentRowIndex, this.currentColIndex);
    node.setPosition(position);
  }

  protected updateGameBlockPosition() {
    this.mapSystem.setBlockPositionOnMap(this.gameBlock, this.currentRowIndex, this.currentColIndex);
  }

  protected abstract _exec(): void;
  protected abstract _undo(): void;
}

export class MoveLeftCommand extends BlockCommand {
  exec(): void {
    if (this.currentColIndex > 0) {
      super.exec();
    }
  }

  _exec(): void {
    this.currentColIndex--;
    cc.tween(this.gameBlock.node)
      .by(CONSTANTS.MOVE_INTERVAL, { x: -this.xOffset })
      .start();
  }

  _undo(): void {
    this.currentColIndex++;
    cc.tween(this.gameBlock.node)
      .by(CONSTANTS.MOVE_INTERVAL, { x: this.xOffset })
      .start();
  }
}

export class MoveRightCommand extends BlockCommand {
  exec(): void {
    if (this.currentColIndex < this.mapSystem.maxColumn - 1) {
      super.exec();
    }
  }

  _exec(): void {
    this.currentColIndex++;
    cc.tween(this.gameBlock.node)
      .by(CONSTANTS.MOVE_INTERVAL, { x: this.xOffset })
      .start();
  }

  _undo(): void {
    this.currentColIndex--;
    cc.tween(this.gameBlock.node)
      .by(CONSTANTS.MOVE_INTERVAL, { x: -this.xOffset })
      .start();
  }
}

export class MoveUpCommand extends BlockCommand {
  exec(): void {
    if (this.currentRowIndex > 0) {
      super.exec();
    }
  }

  _exec(): void {
    this.currentRowIndex--;
    cc.tween(this.gameBlock.node)
      .by(CONSTANTS.MOVE_INTERVAL, { y: this.yOffset })
      .start();
  }

  _undo(): void {
    this.currentRowIndex++;
    cc.tween(this.gameBlock.node)
      .by(CONSTANTS.MOVE_INTERVAL, { y: -this.yOffset })
      .start();
  }
}

export class MoveDownCommand extends BlockCommand {
  exec(): void {
    if (this.currentRowIndex < this.mapSystem.maxRow - 1) {
      super.exec();
    }
  }

  _exec(): void {
    this.currentRowIndex++;
    cc.tween(this.gameBlock.node)
      .by(CONSTANTS.MOVE_INTERVAL, { y: -this.yOffset })
      .start();
  }

  _undo(): void {
    this.currentRowIndex--;
    cc.tween(this.gameBlock.node)
      .by(CONSTANTS.MOVE_INTERVAL, { y: this.yOffset })
      .start();
  }
}