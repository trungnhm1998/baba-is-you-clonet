import GameBlock, { NullGameBlock } from "./game-block";
import { CONSTANTS } from "./constants";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MapSystem extends cc.Component {
  map: Array<Array<GameBlock>>;
  private _originPosition: cc.Vec2;
  private _maxRow: number;
  private _maxColumn: number;


  public get maxRow(): number {
    return this._maxRow;
  }

  public get maxColumn(): number {
    return this._maxColumn;
  }

  onLoad() {
    let canvas = cc.director.getScene().getChildByName("Canvas");
    const left = -((canvas.width / 2)) + (CONSTANTS.BLOCK_WIDTH / 2);
    const top = (canvas.height / 2) - (CONSTANTS.BLOCK_HEIGHT / 2);
    this._originPosition = cc.v2(left, top);

    this._maxColumn = Math.floor(cc.winSize.width / CONSTANTS.BLOCK_WIDTH);
    this._maxRow = Math.floor(cc.winSize.height / CONSTANTS.BLOCK_HEIGHT);
    this.map = new Array<Array<GameBlock>>(this._maxRow);
    for (let rowIndex = 0; rowIndex < this.map.length; rowIndex++) {
      this.map[rowIndex] = new Array<GameBlock>(this._maxColumn);
      for (let colIndex = 0; colIndex < this._maxRow; colIndex++) {
        this.map[rowIndex][colIndex] = NullGameBlock.instance;
      }
    }
  }

  public setBlockPositionOnMap(gameBlock: GameBlock, newRowIndex: number, newColumnIndex: number, setVecPosition = false) {
    if (gameBlock.rowIndex != -1 && gameBlock.columnIndex != -1) {
      this.map[gameBlock.rowIndex][gameBlock.columnIndex] = NullGameBlock.instance;
    }
    this.map[newRowIndex][newColumnIndex] = gameBlock;
    gameBlock.mapSystem = this;
    gameBlock.setMapPosition(newRowIndex, newColumnIndex);

    if (setVecPosition) {
      gameBlock.node.setPosition(this.calculatePositionBasedOnRowAndColumn(newRowIndex, newColumnIndex));
    }
  }

  /**
   * 
   * @param block target block to get from
   * @param isLeftOrRight {left|right} {-1|1}
   * @param isTopOrBottom {top|bottom} {-1|1}
   */
  public getBlockNextToBlock(block: GameBlock, isLeftOrRight: number, isTopOrBottom: number): GameBlock {
    const { columnIndex, rowIndex } = block;
    const getBlockColIndex = columnIndex + isLeftOrRight;
    const getBlockRowIndex = rowIndex + isTopOrBottom;
    if (this.map[getBlockRowIndex] && this.map[getBlockRowIndex][getBlockColIndex]) {
      return this.map[getBlockRowIndex][getBlockColIndex];
    }
    return NullGameBlock.instance;
  }

  public calculatePositionBasedOnRowAndColumn(row: number, column: number): cc.Vec2 {
    const horizontalPadding = (column * CONSTANTS.BLOCK_WIDTH);
    const verticalPadding = (row * CONSTANTS.BLOCK_HEIGHT);
    return cc.v2(this._originPosition.x + horizontalPadding, this._originPosition.y - verticalPadding);
  }
}