import GameBlock from "./game-block";
import { MoveUpCommand, MoveDownCommand, MoveLeftCommand, MoveRightCommand } from "./block-command";

export interface IRule {
  applyRule(gameBlock: GameBlock): void;
}

export class MoveableRule implements IRule {
  private gameBlock: GameBlock;
  applyRule(gameBlock: GameBlock): void {
    this.gameBlock = gameBlock;
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
  }

  onKeyDown(event: cc.Event.EventKeyboard) {
    // cc.log("event", event);
    // let moveToPos: cc.Vec3 = cc.v3();
    switch (event.keyCode) {
      case cc.macro.KEY.up:
      case cc.macro.KEY.w:
        this.gameBlock.execCommand(new MoveUpCommand(this.gameBlock));
        break;
      case cc.macro.KEY.down:
      case cc.macro.KEY.s:
        this.gameBlock.execCommand(new MoveDownCommand(this.gameBlock));
        break;
      case cc.macro.KEY.left:
      case cc.macro.KEY.a:
        this.gameBlock.execCommand(new MoveLeftCommand(this.gameBlock));
        break;
      case cc.macro.KEY.right:
      case cc.macro.KEY.d:
        this.gameBlock.execCommand(new MoveRightCommand(this.gameBlock));
        break;
      case cc.macro.KEY.u:
        this.gameBlock.undoCommand();
        break;
      case cc.macro.KEY.r:
        this.gameBlock.redoCommand();
        break;
    }
  }
}

export class PushableRule implements IRule {
  applyRule(gameBlock: GameBlock): void {
  }
}
