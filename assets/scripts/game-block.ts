import { BlockCommand } from "./block-command";
import { CONSTANTS } from "./constants";
import MapSystem from "./map-system";
import { IRule } from "./rule-interface";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameBlock extends cc.Component {
  @property(cc.Label)
  colIndexLabel: cc.Label = null;

  @property(cc.Label)
  rowIndexLabel: cc.Label = null;

  public mapSystem: MapSystem = null;

  private _commands: Array<BlockCommand>;
  private _undoCommands: Array<BlockCommand>;
  private _columnIndex: number = -1;
  private _rowIndex: number = -1;

  public set rowIndex(index: number) {
    this.rowIndexLabel.string = index + "";
    this._rowIndex = index;
  }

  public get rowIndex() {
    return this._rowIndex;
  }

  public get columnIndex(): number {
    return this._columnIndex;
  }


  public set columnIndex(index: number) {
    this.colIndexLabel.string = index + "";
    this._columnIndex = index;
  }

  public onLoad() {
    this._commands = new Array<BlockCommand>();
    this._undoCommands = new Array<BlockCommand>();

  }

  public setMapPosition(row: number, column: number) {
    this.columnIndex = column;
    this.rowIndex = row;
  }

  public execCommand(command: BlockCommand) {
    this._undoCommands = new Array<BlockCommand>();
    this._commands.push(command);
    command.exec();
  }

  public undoCommand() {
    let lastCommand = this._commands.pop();
    if (lastCommand) {
      lastCommand.undo();
      this._undoCommands.push(lastCommand);
    }
  }

  public redoCommand() {
    let lasUndoCommand = this._undoCommands.pop();
    if (lasUndoCommand) {
      lasUndoCommand.exec();
      this._commands.push(lasUndoCommand);
    }
  }

  public getMoveDistance(): number {
    return this.node.width;
  }

  public applyRule(rule: IRule) {
    rule.applyRule(this);
  }
}

export class NullGameBlock extends GameBlock {
  private static _instance: GameBlock = null;
  static get instance() {
    if (NullGameBlock._instance == null) {
      NullGameBlock._instance = new NullGameBlock();
    }
    return NullGameBlock._instance;
  }

  execCommand(command: BlockCommand) { }
  undoCommand() { }
  redoCommand() { }
}
