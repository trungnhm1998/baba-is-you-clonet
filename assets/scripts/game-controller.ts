import GameBlock from "./game-block";
import MapSystem from "./map-system";
import { MoveableRule, PushableRule } from "./rule-interface";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameController extends cc.Component {
  @property(GameBlock)
  tempBlock: GameBlock = null;

  @property(GameBlock)
  pushableBlock: GameBlock = null;

  @property(MapSystem)
  map: MapSystem = null;

  moveTimer: number = 0;

  onLoad() {
  }

  start() {
    this.map.setBlockPositionOnMap(this.tempBlock, 0, 0, true);
    this.map.setBlockPositionOnMap(this.pushableBlock, 2, 2, true);

    let moveableRule = new MoveableRule();
    let pushableRule = new PushableRule();

    this.tempBlock.applyRule(moveableRule);
    this.pushableBlock.applyRule(pushableRule);
  }
}